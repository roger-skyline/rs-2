## Debops install

# Install python / pip 
sudo apt install build-essential python-dev libffi-dev libssl-dev libsasl2-dev libldap2-dev
sudo apt install python-ldap python-netaddr python-dnspython python-passlib python-pip virtualenv

# Create virtualenv
virtualenv debops ; cd debops
source bin/activate

# Install Debops and ansible
pip install debops[ansible]
