# Add schema in ldap
sudo ldapadd -H ldapi:/// -Y EXTERNAL -f openssh-lpk.ldif

# Backup cron
00 1	* * *	root    /etc/cron.d/backup.sh
00 2	* * *	root    /etc/cron.d/borg-backup.sh

# backup folder
sudo mkdir /var/server.backups/
sudo chown -R ansible:ansible /var/server.backups/
chmod 2770 /var/server.backups/

# backup script
backup.sh est installe sur chaque vm pour creer un tar dans le dossier 
/var/server.backups/ de chaque vm. 

borg-backup.sh monte en sshfs dans /var/servers.backups/ tout les dossiers
/var/server.backups/ de toutes les vms afin de backup les tar dans le dossier 
/var/borg.backup/ .

# PAM LDAP SSH
fetchSSHKeysFromLDAP est un script utilise pour recuperer la public key dans 
le ldap.
