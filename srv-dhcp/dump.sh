#!/usr/bin/env bash
# This script performs a dump from the production readonly database
# and import it into the preprodread-write database
#

# Set up configuration of th import
readonly MONGO_AUTHENTICATION_DATABASE="admin"

readonly MONGO_USERNAME="admin"
readonly MONGO_PASSWORD='PASSWORD'

readonly MONGO_PRODUCTION_HOST='srv-db-slave'
readonly MONGO_PRODUCTION_PORT=2700


# Tell to stop at the first error
set -e

# - Perform a dump of the production database
/opt/mongo-tools/bin/mongodump --ssl \
	--sslPEMKeyFile "/etc/ssl/private/srv-db-slave.slash16.local.pem" \
	--host $MONGO_PRODUCTION_HOST \
	--port $MONGO_PRODUCTION_PORT \
	--username $MONGO_USERNAME \
	--password $MONGO_PASSWORD \
	--authenticationDatabase $MONGO_AUTHENTICATION_DATABASE \
	--out "/tmp/dump.db"
