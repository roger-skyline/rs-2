SSL
------

```
openssl version: 1.1
openssl bit: 2048 
openssl config path: /root/ca
```

A SSL certificate authority has been created to generate all SSL certificate of the infrastructure.
Intermidiates CA has been created to generate SSL certificates for all machines.    
Root and intermediate CA are places in Machine 2 (dhcp.slash16.local)
   
An administrator email has been define along all default conf like FQDN, organistation name, country name ...    
Every hosts use a pair of certificate (crt, key) for all of it services.
