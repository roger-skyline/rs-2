Backup
------

```
Borg: 1.0.9  
Etckeeper: 1.18.5
System command: scp
```

### Etckeeper
Package install by ansible with Debops on every machines.  
Git remote configured is gogs with etckeeper for user.  
Etckeeper install a script in `/etc/cron.daily`, so everyday `/etc` directory changes are commit and push to `gogs.slash16.local`

### General backup
On each hosts a bash script is placed in `/etc/cron.d/` and backup in a tar file every critic directories / files.  
The script is executed everyday at `1am`. So borg, on the srv-dhcp server, can pick up at `2am` every tar file placed in `/var/server.backups` in all VMs.
The script, borg-backup.sh, use sshfs mount point to backup all tar file. It makes a replication on srv-worker-2 through ssh with rsync.
