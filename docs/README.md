# Documentations

#### Automatisation
- [Ansible](https://github.com/ansible/ansible) (2.7.6)  
- [Debops](https://github.com/debops/debops) (0.8)  
- [Ansible-role-security](https://github.com/geerlingguy/ansible-role-security) (1.8.0) forked and edited  
- [Ansible-role-firewall](https://github.com/geerlingguy/ansible-role-firewall) (2.4.1) forked and edited

Security
------
[Firewall]()  
[SSH]()

### Authenticaton

[LDAP](https://github.com/Roger-skyline/RS-2/tree/master/docs/ldap)

## Managments

[Automatisation](https://github.com/Roger-skyline/RS-2/tree/master/docs/ansible)  
[User]()

Maintainment
------
### Backup
### Monitoring
[Shinken](https://github.com/naparuba/shinken)
[Checks](https://github.com/shinken-monitoring)

## Application

### Pre-Production
[Database]()  
[Worker]()

### Production
[Database]()  
[Worker]()

### Mail  
[Postfix]()  
[Dovecot]()
