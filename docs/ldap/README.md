LDAP
------

```
libldap version: 2.4-2
protocol: ldaps
Install location: VM 2
domain name: ldap.slash16.local
```

LDAP handle user's authentications of our infrastructure.
Only user with public key configured in LDAP can connect throw ssh on machines.    
Multiple services use for user's auth:
 - Mail (dovecot)
 - Versioning (gogs)
 - XMPP
 - Monitoring (shinken)
 - FTP

Each time a user is created in LDAP database, an acount in each service listed before is created.
Some services use LDAP user like:
 - etckeeper (for versioning)
 - shiken (used by shinken services)
