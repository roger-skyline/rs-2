Services check
------

### Global system
#### SSH
- [ ] ssh must allow only ldap user with ssh key
- [ ] ssh connection must be done from machine 1,2,3 to all VMs
- [ ] ssh connection from outsite is possible on every VM throw port forwading

#### Authentication (LDAP)
- [ ] All LDAP user have meaning
- [ ] LDAP service must use LDAPs protocol
- LDAP user password:
    1. [ ] Different from default
    2. [ ] longer than 8 char

#### System
- [ ] root authentication is only possible by privilege escalation
- [ ] root password has been change and longuer or egal to 16 char
- [X] Packages are up to date 2 3 4 5 6 7 8 9 10
- [ ] No useless package are installed
- [ ] Old distribution kerbel version are removed
- [ ] ONLY admin account must be present

### Network
#### DNS
- [x] All domain name are coherent
- [ ] Reverse dns is working on every IPv4
- [ ] Every machine has the same `/etc/resolv.conf` (no custom configuration)
- [ ] Every machine has the same `/etc/hosts` (no custom configuration)
- [ ] More checks ?

#### DHCP
- [x] IP range given by server is the same as in the subject
- [ ] More checks ?

#### Firewall
- [ ] On NAT machine all outside open port are usefull and point to a working service
- [ ] On every machines only machine 1,2 and 3 can ssh to them and only used port are open
- [ ] INPUT and FORWARD firewall are in DROP POLICY

### Workers
- [ ] 2 workers are running on a custom port with ssl activate
- [ ] a loadbalancer is running on NAT machine in roundrobin (with ssl installed too)
- [ ] If a worker die the second take all the charges
- [ ] If Loadbalancer die all charge are sent to an unique worker
- [ ] It is possible to simply push preproduction code to production
- [ ] Workers are reload automaticaly when code new code are push to them

### Databases
- [ ] There are 2 databases, one in RW and another in Read only (replication)
- [ ] Database communication use SSL certificates
- [ ] It is possible to simply put production database data into preproduction
- [ ] If one database goes down the application work in read-only

### Preproduction
- [ ] Web application must have a repository with 2 branchs (master and preproduction)
- [ ] A worker is running in the "same" conditions as production
- [ ] A database is running in the "same" conditions as production

###  Communication
#### Mails
- [ ] Mails protocols used are POP3s, IMAPs and SMTPs
- [ ] Any users who have an account in LDAP, have an account in mail database
- [ ] Users can consult their mails in any VMs
- [ ] Mails are used by Gogs (versioning)
- [ ] Mails are used by Borg to send warnings ?

#### XMPP
- [X] A xmpp server is running and use an encrypted protocol
- [X] XMPP server is available from outside
- [X] Any users who have an account in LDAP, have an account in XMPP service

### Monitoring
- [X] Monitoring service is running with a graphic interface
- [X] Graphic interface use an encrypted protocols and is available from outside
- [X] Must send a notification for every service unavailable:
    0. [X] Linux base checks (every hosts)
    1. [X] DHCP service down
    2. [X] DNS service down
    3. [X] LDAP service down
    4. [X] XMPP service down
    5. [X] Any VM down (ping test)
    6. [X] Worker 1 service down (https check)
    7. [X] Worker 2 service down (https check)
    8. [X] Master databse down
    9. [X] Slave database down
    10. [X] Preprod worker down (https check)
    11. [X] Preprod database down
    12. [X] SMTPs service down
    13. [X] POP3s service down
    14. [X] IMAPs service down
    15. [X] Versioning service down (https check)
    16. [X] FTP service down
    17. [X] Htpps checks
    18. [X] Certificate checks

### Versionning
- [ ] A versioning service is working throw https and ssh protocols on every machines
- [ ] Only specific users must have an account (these users must be in LDAP)

### Stockage
#### Borg
- [x] A client script is configured to make a local backup every day
- [x] Borg must take every local backup from each VMs and then compress them

#### FTP
- [ ] FTP service use FTPs protocols
- [ ] Any users who have an account in LDAP, have an account in FTP service
- [ ] FTP service is available outside

#### Etckeeper
- [ ] etckeeper service is working on every machines
- [ ] etckeeper have an account in versionning service
- [ ] Every repository it create must be private and have hostname name
- [ ] Every day a commit must be pushed (if there are changes) to remote with etc modifications
