#!/bin/bash

openssl rand -base64 756 > secret
chmod 440 secret

roles=( mongod mongoc mongos )
for x in "${roles[@]}"
do
	cp secret database/roles/$x/files
done
