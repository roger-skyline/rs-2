# GATEWAY

## Enable ip forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward

### Forward traffic throw eth0 from eht1 to others VM
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT

## Add iptables configuration (DROP policies for INPUT AND FORWARD)
